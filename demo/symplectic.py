import os
import numpy as np
import numpy.testing as npt
from dataclasses import dataclass
from rich.progress import Progress
import matplotlib.pyplot as plt
from matplotlib.animation import FFMpegFileWriter

import jax

import jax.numpy as jnp

import micromage as mg
print(f"{mg.dtype=}")

#########################################################################

def ansatz(params, xy):
    x0, y0, radius, width, chirality, skew, torsion = params
    # DOI: 10.1038/s42005-018-0029-0
    x, y = xy[0]-x0, xy[1]-y0 # Cartesian coordinate in place centered at the skyrmion.
    rho2 = x**2+y**2 + 1e-12 #
    rho = jnp.sqrt(rho2) # Polar radius.
    x, y = x/rho, y/rho # Direction outside the skyrmmion.
    n_theta = 2*jnp.arctan2(jnp.sinh(radius/width), jnp.sinh(rho/width)) # Ansatz skyrmion profile.
    nz, nxy = jnp.cos(n_theta), jnp.sin(n_theta) # Out of plane and in plane projections of magnetization.
        
    # n_phi = nu*phi+gamma,  
    # where nu is vorticity (nu = 1 for a skyrmion and nu = −1 for an antiskyrmion), 
    # and gamma is a constant classifying type of skyrmions=chirality 
    # (gamma=0 or pi for Néel skyrmions and gamma = +-pi/2 for Bloch skyrmions).
    dr = radius-rho
    angle = chirality + dr*skew + dr*jnp.arctan(dr)*torsion
    c, s = jnp.cos(angle), jnp.sin(angle)
    nx = nxy*(c*x+s*y) # x-projection
    ny = nxy*(-s*x+c*y) # y-projection of magnetization.
    n1 = jnp.stack([nx,ny,nz],axis=0) # 1st layer.
    return n1[:,:,:,None] # Single layer state.

def ansatz_s(params, xy):
    x0, y0, radius, scale, chirality, skew, torsion = params
    x, y = xy[0]-x0, xy[1]-y0 # Cartesian coordinate in place centered at the skyrmion.
    rho2 = x**2+y**2 + 1e-12 #
    rho = jnp.sqrt(rho2) # Polar radius.
    x, y = x/rho, y/rho # Direction outside the skyrmmion.
    dr = radius-rho    
    n_theta = 2*jnp.arctan(jnp.exp(dr*scale)) # Ansatz skyrmion profile.
    nz, nxy = jnp.cos(n_theta), jnp.sin(n_theta) # Out of plane and in plane projections of magnetization.
        
    # n_phi = nu*phi+gamma,  
    # where nu is vorticity (nu = 1 for a skyrmion and nu = −1 for an antiskyrmion), 
    # and gamma is a constant classifying type of skyrmions=chirality 
    # (gamma=0 or pi for Néel skyrmions and gamma = +-pi/2 for Bloch skyrmions).
    angle = chirality + dr*skew + dr**2*torsion
    c, s = jnp.cos(angle), jnp.sin(angle)
    nx = nxy*(c*x+s*y) # x-projection
    ny = nxy*(-s*x+c*y) # y-projection of magnetization.
    n1 = jnp.stack([nx,ny,nz],axis=0) # 1st layer.
    return n1[:,:,:,None] # Single layer state.    

def len_error(n):
    return np.max(np.abs(1-np.sum(n**2,axis=0)))

###############################################################################
# LLG

###############################################################################
# Relax

system = mg.System(
    Lx=18, Ly=18, D=3.5, K=7.7, H=0,
    eta=0.2, jx=0, beta=0.,
)
print(system)

comp = mg.Computer2(system)
xy = comp.coordinates(900, 900)

folder =  f'../tmp8/rlx_Lx{system.Lx}_D{system.D}_K{system.K}_H{system.H}_eta{system.eta}_sx{xy.shape[1]}'
os.makedirs(folder, exist_ok=True)

# Full ansatz
a = mg.Ansatz(ansatz, computer=comp, args=[xy])
p0 = jnp.array([system.Lx/2,system.Ly/2,2.5,0.357,-np.pi/2,0.,0.])


filename = f'{folder}/stbl'
print(f"Saving to {filename}.npz")
n0 = a(p0)
n1 = mg.run_llg_dynamics(filename, n0=n0, comp=comp, 
    fit=lambda n: mg.fit_params(n, xy), 
    plot=lambda n, **kwargs: mg.analyze_geometry(n, xy, **kwargs),
    simulation_period=1., dt=0.00003, report_time=0.006, rep=2,
)
    
# analyze_geometry(n1, xy)

data, _ = mg.plot_result(f"{filename}.npz")
plt.savefig(f"{filename}.summary.pdf")
# plt.show()

###############################################################################
# Apply

comp.system = system = mg.System(
    Lx=18, Ly=18, D=3.5, K=7.7, H=0.1,
    eta=0.2, jx=0, beta=0.,
)
print(comp.system)
  
filename = f'{folder}/aply'
print(f"Saving to {filename}.npz")
n2 = mg.run_llg_dynamics(filename, n0=n1, comp=comp, 
    fit=lambda n: mg.fit_params(n, xy), 
    plot=lambda n, **kwargs: mg.analyze_geometry(n, xy, **kwargs),
    simulation_period=20., dt=0.00003, report_time=0.02
)

# analyze_geometry(n2, xy)

data, _ = mg.plot_result(f"{filename}.npz")
plt.savefig(f"{filename}.summary.pdf")
plt.show()

#################################################################################
# Relaxation

comp.system = system = mg.System(
    Lx=18, Ly=18, D=3.5, K=7.7, H=0.,
    eta=0.2, jx=0, beta=0.,
)
print(comp.system)

data = np.load(f'{folder}/aply.npz')
n2 = data['n_llg']

filename = f'{folder}/relx'
print(f"Saving to {filename}.npz")
n3 = mg.run_llg_dynamics(filename, n0=n2, comp=comp, 
    fit=lambda n: mg.fit_params(n, xy), 
    plot=lambda n, **kwargs: mg.analyze_geometry(n, xy, **kwargs),
    simulation_period=40., dt=0.00003, report_time=0.02
)

# analyze_geometry(n3, xy)

data, _ = mg.plot_result(f"{filename}.npz")
plt.savefig(f"{filename}.summary.pdf")
plt.show()
