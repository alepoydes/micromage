from setuptools import setup

setup(name='micromage',
    version='0.0.1',
    description='Simulator of layered micromagnetic systems.',
    url='https://gitlab.com/alepoydes/micromage',
    author='Igor Lobanov',
    author_email='lobanov.igor@gmail.com',
    license='GPLv3',
    packages=['micromage'],
    install_requires=[
        'numpy', 'matplotlib', 'jax', 'jax_dataclasses',
        'rich',
        ],
    test_suite='tests',
    zip_safe=False,
    python_requires='>=3.8.0',
    )