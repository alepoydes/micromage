import jax.numpy as jnp
import jax
# import numpy as np

@jax.jit
def center_of_magnetization(n, xy):
    assert n.ndim==3, "State vector `n` should be restricted to single layer `n[...,layer]`"
    density = n[2]-1
    mass = jnp.sum(density)
    x, y = xy
    cx = jnp.sum(x*density)/mass
    cy = jnp.sum(y*density)/mass
    return jnp.asarray([cx, cy])

@jax.jit
def extract_profile(n, center, xy, flatten=True):
    assert n.ndim==3, "State vector `n` should be restricted to single layer `n[...,layer]`"
    nz = n[2]
    x, y = xy[0] - center[0], xy[1] - center[1]
    r = jnp.sqrt(x**2 + y**2)
    if flatten:
        return r.flatten(), nz.flatten()
    return r, nz

@jax.jit
def compute_radius(r, nz, minz=0, maxz=2):
    # Compute tg(n_theta)^2.
    b = (1+nz)/(1-nz)
    # Filter too far
    maxr = jnp.max(r)
    # Filter region where asymptotics is invalid.
    msk = jnp.logical_and(r<maxr/2, jnp.logical_and(b>10**minz, b<10**maxz))
    r = jnp.where(msk,r,jnp.zeros_like(r))
    b = jnp.where(msk,b,jnp.ones_like(b))
    # To logarithmic scale.
    f = jnp.log(b)
    # Linear fit.
    x = jnp.stack([r,jnp.where(msk,jnp.ones_like(r),jnp.zeros_like(r))],axis=1)
    k, c = jnp.linalg.lstsq(x, f,rcond=None)[0]
    
    # DEBUG
    # plt.plot(r, f, ',k')
    # r0 = jnp.linspace(0, jnp.max(r), 100)
    # plt.plot(r0, r0*k+c, '-r')
    # plt.show()
    # END DEBUG
    
    # Transform to radius and domain wall width.
    rad = -c/k # radius
    w = 2/k # width
    return rad, w

@jax.jit
def extract_angle(n, center, xy, zthreshold=0.8):
    assert n.ndim==3, "State vector `n` should be restricted to single layer `n[...,layer]`"
    # Polar angle for magnetization
    nx, ny, nz = n
    nz = nz.flatten()
    w = jnp.where(jnp.logical_and(nz>-zthreshold, nz<zthreshold), jnp.ones_like(nz), jnp.zeros_like(nz))
    n_phi = jnp.arctan2(ny, nx).flatten()
    # Polar angle for coordinates.
    x, y = xy[0] - center[0], xy[1] - center[1]
    phi = jnp.arctan2(y, x).flatten()
    # Sort by angle
    idx = jnp.argsort(phi)
    return phi[idx], n_phi[idx], w[idx]

@jax.jit
def extract_chirality_profile(n, center, xy, zthreshold=0.8):
    assert n.ndim==3, "State vector `n` should be restricted to single layer `n[...,layer]`"
    nx, ny, nz = n
    nz = nz.flatten()
    w = jnp.where(
        jnp.logical_and(nz>-zthreshold, nz<zthreshold), 
        jnp.ones(nz.shape,dtype=bool), 
        jnp.zeros(nz.shape,dtype=bool)
        )
    # Polar angle for magnetization
    n_phi = jnp.arctan2(ny, nx).flatten()
    # Polar angle for coordinates.
    x, y = xy[0] - center[0], xy[1] - center[1]
    r = jnp.sqrt(x**2+y**2).flatten()
    phi = jnp.arctan2(y, x).flatten()
    # Difference of angles in [-pi,pi].``
    dphi = (n_phi-phi)/(2*jnp.pi)+0.5
    dphi = jnp.ceil(dphi)-dphi
    dphi = (dphi-0.5)*(2*jnp.pi)
    return r, dphi, w

@jax.jit
def compute_chirality(phi, n_phi, w, thr=1.5):
    # [-pi,pi] -> [-1,1]
    phi = phi/(jnp.pi*2)
    n_phi = n_phi/(jnp.pi*2)
    diff = phi-n_phi
    diff = jnp.ceil(diff)-diff
    c = jnp.sum(diff*w)/jnp.sum(w)
    c = jnp.where(c>0.5, c-1, c)
    chi = c*jnp.pi*2
        
    q = 1
    return chi, q

@jax.jit
def compute_chirality_skew(r, dphi, w, radius):
    y = jnp.where(w, dphi, jnp.zeros_like(dphi))
    dr =jnp.where(w, radius-r, jnp.zeros_like(r)) 
    x = jnp.stack([
        dr**2,
        dr,
        jnp.where(w, jnp.ones_like(r), jnp.zeros_like(r))
        ],axis=1)
    torsion, skew, chi = jnp.linalg.lstsq(x, y)[0]
    return chi, skew, torsion


# @jax.jit
# def compute_chirality(phi, n_phi, w, thr=1.5):
#     # [-pi,pi] -> [-1,1]
#     phi = phi/jnp.pi+1
#     n_phi = n_phi/jnp.pi+1
#     # Unwrap (to a monotonic function)
#     diff = jnp.diff(n_phi, 1, axis=0)
#     diff = jnp.where(diff>thr, diff-2, diff)
#     diff = jnp.where(diff<-thr, diff+2, diff)
#     res = jnp.cumsum(diff) + n_phi[0]
#     # Linear fit.
#     x = jnp.stack([phi[1:],jnp.ones(res.shape)],axis=1)
#     x = x*w[:,None]
#     res = res*w
#     q, c = jnp.linalg.lstsq(x, res)[0]
#     # Compute chirality
#     c = jnp.where(c>1, c - 2, c)
#     chi = c*jnp.pi
        
#     # DEBUG
#     # plt.plot(phi[1:], diff, '.k')
#     # plt.plot(phi, n_phi, '.b')   
#     # plt.plot(phi[1:], res, ',r')       
#     # plt.show()
#     # END DEBUG
    
#     return chi, q


@jax.jit
def fit_params_old(n, xy):
    ns = n[...,0]
    center = center_of_magnetization(ns, xy)
    r, nz = extract_profile(ns, center, xy)
    rad, wdt = compute_radius(r, nz) 
    phi, n_phi, w = extract_angle(ns, center, xy)
    chi, q = compute_chirality(phi, n_phi, w)
    return jnp.array([center[0],center[1],rad,chi,wdt,0.])

@jax.jit
def fit_params(n, xy):
    ns = n[...,0]
    center = center_of_magnetization(ns, xy)
    r, nz = extract_profile(ns, center, xy)
    rad, wdt = compute_radius(r, nz) 
    r1, dphi, w = extract_chirality_profile(ns, center, xy)
    chi, skew, torsion = compute_chirality_skew(r1, dphi, w, rad)
    return jnp.array([center[0],center[1],rad,wdt,chi,skew,torsion])

