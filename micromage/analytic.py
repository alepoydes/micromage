import numpy as np

from .basics import System
from .cfg import dtype

#############################################################################

class AnalyticA:
    def __init__(self, system: System=None, xy=None):
        self.system = system
        self.xy = xy
        # self.d_energy = jax.jit(jax.grad(self.energy, argnums=0))

    def ansatz(self, pars, xy=None):
        x0, y0, rad, wdt, chi, _, _ = np.array(pars, dtype=dtype)

        if xy is None: xy=self.xy
        x, y = xy[0]-x0, xy[1]-y0

        rho = np.sqrt(x**2+y**2)
        phi = np.arctan2(y, x)
        psi = phi - chi
        theta = self.theta(rho, rad,wdt)
        nx = np.cos(psi)*np.sin(theta)
        ny = np.sin(psi)*np.sin(theta)
        nz = np.cos(theta)
        return np.stack([nx,ny,nz],axis=0)[...,None]

    def v_d_ansatz(self, pars, xy=None):
        x0, y0, rad, wdt, chi, _, _ = np.array(pars, dtype=dtype)

        if xy is None: xy=self.xy
        x, y = xy[0]-x0, xy[1]-y0

        rho = np.sqrt(x**2+y**2)
        phi = np.arctan2(y, x)
        psi = phi - chi
        theta = self.theta(rho, rad, wdt)
        nx = np.cos(psi)*np.sin(theta)
        ny = np.sin(psi)*np.sin(theta)
        nz = np.cos(theta)

        nx_theta = np.cos(psi)*np.cos(theta)
        ny_theta = np.sin(psi)*np.cos(theta)
        nz_theta = -np.sin(theta)
        nx_psi = -np.sin(psi)*np.sin(theta)
        ny_psi = np.cos(psi)*np.sin(theta)

        rho_x0, rho_y0 = -x/rho, -y/rho
        phi_x0 = y/(x**2+y**2) 
        phi_y0 = -x/(x**2+y**2) 

        theta_rho, theta_rad, theta_wdt = self.theta_rho(rho,rad,wdt), self.theta_rad(rho,rad,wdt), self.theta_wdt(rho,rad,wdt)

        nx_x0 = nx_theta*theta_rho*rho_x0 + nx_psi*phi_x0
        ny_x0 = ny_theta*theta_rho*rho_x0 + ny_psi*phi_x0
        nz_x0 = nz_theta*theta_rho*rho_x0

        nx_y0 = nx_theta*theta_rho*rho_y0 + nx_psi*phi_y0
        ny_y0 = ny_theta*theta_rho*rho_y0 + ny_psi*phi_y0
        nz_y0 = nz_theta*theta_rho*rho_y0

        nx_rad = nx_theta*theta_rad 
        ny_rad = ny_theta*theta_rad 
        nz_rad = nz_theta*theta_rad

        nx_wdt = nx_theta*theta_wdt 
        ny_wdt = ny_theta*theta_wdt 
        nz_wdt = nz_theta*theta_wdt

        nx_chi = -nx_psi
        ny_chi = -ny_psi

        z = np.zeros_like(nx_x0)

        return np.array([nx,ny,nz])[...,None], np.array([
                [nx_x0, nx_y0, nx_rad, nx_wdt, nx_chi, z, z],
                [ny_x0, ny_y0, ny_rad, ny_wdt, ny_chi, z, z],
                [nz_x0, nz_y0, nz_rad, nz_wdt, z,      z, z],
            ], dtype=z.dtype)[...,None]

    def theta(self, rho, rad, wdt):
        q = self.q(rho, rad, wdt)
        theta = 2*np.arctan(q)
        return theta
            
    def theta_rho(self, rho, rad, wdt):
        return 2/(1+self.q(rho, rad, wdt)**2)*self.q_rho(rho, rad, wdt)

    def theta_rad(self, rho, rad, wdt):
        return 2/(1+self.q(rho, rad, wdt)**2)*self.q_rad(rho, rad, wdt)

    def theta_wdt(self, rho, rad, wdt):
        return 2/(1+self.q(rho, rad, wdt)**2)*self.q_wdt(rho, rad, wdt)

    def __call__(self, p):
        return self.ansatz(p)
        
    def q(self, rho, rad, wdt):
        return np.sinh(rad/wdt)/np.sinh(rho/wdt)

    def q_rho(self, rho, rad, wdt):
        return -np.sinh(rad/wdt)/np.sinh(rho/wdt)**2*np.cosh(rho/wdt)/wdt

    def q_rad(self, rho, rad, wdt):
        return np.cosh(rad/wdt)/np.sinh(rho/wdt)/wdt

    def q_wdt(self, rho, rad, wdt):
        return (
            -rad*np.cosh(rad/wdt)/np.sinh(rho/wdt)
            +rho*np.sinh(rad/wdt)/np.sinh(rho/wdt)**2*np.cosh(rho/wdt)
        )/wdt**2
    
    def energy(self, pars):
        _,_,rad,wdt,chi,_,_=pars
        si = self.ansatz(pars, xy=[self.system.imp_x,self.system.imp_y])
        return 2*np.pi*( 
            self.system.A*self.I1(rad, wdt)
            +(self.system.Db*np.sin(-chi)+self.system.Di*np.cos(chi))*self.I2(rad, wdt)
            +self.system.K*self.I3(rad, wdt)
            +self.system.H*self.I4(rad, wdt)
            ) + np.sum(self.system.imp_K[:,None]*si[2]**2)
    
    def d_energy(self, pars):
        _,_,rad,wdt,chi,_,_=pars
        de_vol = 2*np.pi*np.array([
            0, # d x0
            0, # d y0
            self.system.A*self.I1_rad(rad, wdt)
                +(self.system.Db*np.sin(-chi)+self.system.Di*np.cos(chi))*self.I2_rad(rad, wdt)+
                +self.system.K*self.I3_rad(rad, wdt)+self.system.H*self.I4_rad(rad, wdt), # d rad
            (self.system.A*self.I1_wdt(rad, wdt)
                +(self.system.Db*np.sin(-chi)+self.system.Di*np.cos(chi))*self.I2_wdt(rad, wdt)+
                +self.system.K*self.I3_wdt(rad, wdt)+self.system.H*self.I4_wdt(rad, wdt)), # d wdt
            (-self.system.Db*np.cos(chi)-self.system.Di*np.sin(chi))*self.I2(rad, wdt), # d chi
            0,
            0,
            ])

        si, di = self.v_d_ansatz(pars, xy=[self.system.imp_x,self.system.imp_y])
        t = self.system.imp_K[None,:,None]*2*si[2,None]*di[2]
        # print(f"{si.shape=} {di.shape=} {t.shape=}")
        de_imp = np.sum(t, axis=(1,2))

        # print(f"{de_vol=}")
        # print(f"{de_imp=}")

        return de_vol+de_imp
    
    def I1(self, rad, wdt):
        """int_0^infty [(d theta/d r)^2+sin^2(theta)/r^2] r dr"""
        x = rad/wdt
        # a = np.pi/x
        # return 2*(x**2+1+a**2/12+7/240*a**4+31/1344*a**6+127/3840*a**8)/x
        return 2*(x+1/x+np.pi**2/x**3/12+np.pi**4*7/240/x**5+31/1344*np.pi**6/x**7+127/3840*np.pi**8/x**9)

    
    def I1_rad(self, rad, wdt): 
        x = rad/wdt
        return -2*(-1 + 1/x**2 + 3*np.pi**2/x**4/12 + 5*np.pi**4*7/240/x**6 + 7*31/1344*np.pi**6/x**8
            + 9*127/3840*np.pi**8/x**10)/wdt
        # return 2*(1/wdt-wdt/rad**2)
 
    def I1_wdt(self, rad, wdt): 
        x = rad/wdt
        return 2*(-1 + 1/x**2 + 3*np.pi**2/x**4/12 + 5*np.pi**4*7/240/x**6 + 7*31/1344*np.pi**6/x**8
            + 9*127/3840*np.pi**8/x**10)*rad/wdt**2        
        # return self.I1_rad(wdt,rad)
    
    def I2(self, rad, wdt):
        """int_0^infty [d theta/d r+sin(2 theta)/(2 r)] r dr"""
        return -np.pi*rad
    
    def I2_rad(self, rad, wdt): return -np.pi
    def I2_wdt(self, rad, wdt): return 0
    
    def I3(self, rad, wdt):
        """int_0^infty sin^2(theta) r dr"""
        return 2*wdt*rad

    def I3_rad(self, rad, wdt): return 2*wdt
    def I3_wdt(self, rad, wdt): return 2*rad

    
    def I4(self, rad, wdt):
        """int_0^infty (1-cos(theta)) r dr"""
        return 2*(rad**2/2+np.pi**2/24*wdt**2)

    def I4_rad(self, rad, wdt): return 2*rad
    def I4_wdt(self, rad, wdt): return np.pi**2/6*wdt

    def equation(self, pars):
        _,_,rad,wdt,chi,_,_ = pars 
        dE = self.d_energy(pars)
        
        i1 = np.pi*self.I1(rad,wdt)
        i3 = 2*np.pi*self.I3(rad,wdt)
        ww = 2*np.pi*(np.pi**2/6*rad/wdt)
        dw = 2*np.pi*(np.pi**2/6) # Does not depend on R and wdt.
        zeta = wdt#*np.tanh(rad/wdt)
        A = np.array([
                [i1,0 ,0         ,0 ,0 ,0,0],
                [0 ,i1,0         ,0 ,0 ,0,0],
                [0 ,0 ,i3/zeta**2,dw,0 ,0,0],
                [0 ,0 ,dw        ,ww,0 ,0,0],
                [0 ,0 ,0         ,0 ,i3,0,0],
                [0 ,0 ,0         ,0 ,0 ,0,0],
                [0 ,0 ,0         ,0 ,0 ,0,0],            
            ])
        
        q = 4*np.pi
        B = np.array([
                [0 ,q,0       ,0       ,0      ,0,0],
                [-q,0,0       ,0       ,0      ,0,0],
                [0 ,0,0       ,0       ,i3/zeta,0,0],
                [0 ,0,0       ,0       ,dw*zeta,0,0],
                [0 ,0,-i3/zeta,-dw*zeta,0      ,0,0],
                [0 ,0,0       ,0       ,0      ,0,0],
                [0 ,0,0       ,0       ,0      ,0,0],            
            ])
                        
        jx = -A[0]
        jy = -A[1]
        jx2 = B[0]
        jy2 = B[1]
        return A,B,dE,jx,jy,jx2,jy2
    
    def time_derivative(self, params):
        s = self.system
        A,B,dE,jx,jy,jx2,jy2 = self.equation(params)
        mat = (s.eta*s.Ms)*A + 1/s.gamma*B
        rhs = -(1/s.Ms)*dE + s.jx*jx2+s.jy*jy2 + s.jx*s.beta*jx+s.jy*s.beta*jy 
        return np.linalg.lstsq(mat, rhs, rcond=None)[0]

    def time_increment(self, p, dt):
        # Runge-Kutta method.
        k1 = self.time_derivative(p)
        k2 = self.time_derivative(p+0.5*dt*k1)
        k3 = self.time_derivative(p+0.5*dt*k2)
        k4 = self.time_derivative(p+dt*k3)
        return p+dt/6*k1+dt/3*k2+dt/3*k3+dt/6*k4

    
class AnalyticB(AnalyticA):
    def equation(self, pars):
        A,B,dE,jx,jy,jx2,jy2 = super().equation(pars)
        dE[3]=0
        A[3,:] = 0; A[:,3] = 0; A[3,3] = 1
        B[3,:] = 0; B[:,3] = 0; 
        jx[3] = jy[3] = jx2[3] = jy2[3] = 0
        return A,B,dE,jx,jy,jx2,jy2 
    
class AnalyticR(AnalyticA):
    def equation(self, pars):
        A,B,dE,jx,jy,jx2,jy2 = super().equation(pars)
        dE[3:5]=0
        A[3:5,:] = 0; A[:,3:5] = 0; 
        # A[3,3] = 1; A[4,4] = 1
        B[3:5,:] = 0; B[:,3:5] = 0; 
        jx[3] = jy[3] = jx2[3] = jy2[3] = 0
        jx[4] = jy[4] = jx2[4] = jy2[4] = 0
        # print("A=",np.round(A,2))
        # print("B=",np.round(B,2))
        # print(f"{np.round(dE,2)=}")
        # print(f"{np.round(jx,2)=}")
        # print(f"{np.round(jy,2)=}")
        # print(f"{np.round(jx2,2)=}")
        # print(f"{np.round(jy2,2)=}")
        # exit()
        return A,B,dE,jx,jy,jx2,jy2 