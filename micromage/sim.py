import numpy as np
from rich.progress import Progress
import matplotlib.pyplot as plt
from matplotlib.animation import FFMpegFileWriter
import jax.numpy as jnp
import jax


from .basics import System, Computer, Computer2, Ansatz, Thiele, CDyn


def run_llg_dynamics(
        filename, n0, comp:Computer, fit, plot,
        simulation_period=0.1, dt=0.0001, report_time=0.001,
        figsize=(15,5), **kwargs,
        ):
    # Initial state
    t = 0.
    last_report = t
    
    n_llg = n0
    
    # Init data collection
    hist_llg = [fit(n_llg)]
    energy_llg = [comp.energy(n_llg)]
    time = [t]

    # Init graphics
    moviewriter = FFMpegFileWriter(fps=23.98, codec='h264')

    figax = plot(n_llg, figsize=figsize)

    with moviewriter.saving(figax[0], f'{filename}.mp4', dpi=100):

        with Progress(transient=True) as progress:
            task = progress.add_task("Working", total=simulation_period)

            try:
                while t<simulation_period:
                    n_llg = comp.time_increment(n_llg, dt=dt, **kwargs)

                    t += dt
                    if t>last_report+report_time:
                        last_report = t

                        p_llg = fit(n_llg)
                        e_llg = comp.energy(n_llg)
                        
                        progress.update(task, completed=t)
#                         progress.console.print(f"{t:.4f} [green]LLG   [/] Energy {e_llg:.3f} Len. error {len_error(n_llg)}")
                        
                        time.append( t )
                        hist_llg.append( p_llg )
                        energy_llg.append( e_llg )                        

                        plot(n_llg, figax=figax, time=t)

                        moviewriter.grab_frame()
            except KeyboardInterrupt:
                print(f"Terminated.")        
        print(f"Finalizing the movie.")
    print(f"Done")

    hist_llg = np.array(hist_llg)
    energy_llg = np.array(energy_llg)    
    time = np.array(time)

    np.savez(f'{filename}.npz', 
             system=comp.system.__dict__, 
             time=np.array(time),
             hist_llg=np.array(hist_llg),
             energy_llg=np.array(energy_llg), 
             n0 = np.asarray(n0),
             n_llg = np.asarray(n_llg),
            )
    
    return n_llg


#########################################################################################################

def run_thiele_dynamics(
        state, ansatz:Ansatz, 
        comp: Computer,
        simulation_period=0.1, dt=0.0002, report_time=0.02,
        decode=lambda p:p,
        ):
    # state = jnp.array(state)
    a = ansatz
    # Initial state
    t = 0.
    last_report = t
    
    # Init data collection
    p = decode(state)
    hist = [list(p)]    
    energy = [a.energy(state)]
    time = [t]

    # Init graphics
    with Progress(transient=True) as progress:
        task = progress.add_task("Working", total=simulation_period)

        try:
            while t<=simulation_period:
                state = a.time_increment(state, dt=dt)

                t += dt
                if t>last_report+report_time:
                    last_report = t

                    # n = a(state)
                    # e = comp.energy(n)

                    e = a.energy(state)

                    progress.update(task, completed=t)

                    time.append( t )
                    p = decode(state)
                    hist.append( list(p) )
                    energy.append( e )
        except KeyboardInterrupt:
            pass
    return np.asarray(time), np.asarray(hist), np.asarray(energy)


def execute_experiment(filename, fit, ansatz=None, ansatz_fn=None, 
                       save_state=lambda _:[], encode=lambda p:p, decode=lambda p:p, 
                       name=None, dt=0.0002, thiele=True):
    if name is None:
        name = f'thiele'
    print(f"Saving to {name}")
    # Read file creare by `run_llg_dynamics`
    data = np.load(f"{filename}.npz", allow_pickle=True)
    time = data['time']
    n0 = data['n0']
    # Recreate system
    system = System(**data['system'][()])
    print(f"Loaded system {system}")
    print(f"Data shape {n0.shape}")    
    print(f"Read parameters {data['hist_llg'][0]}")    
    comp = Computer2(system)
    xy = comp.coordinates(*n0.shape[1:3])
    p0 = np.array(fit(n0,xy))
    print(f"Computed parameters {p0}")   
    e0 = encode(p0)
    print(f"Encoded parameters {e0}")
    d0 = decode(e0)
    print(f"Decoded parameters {d0}")
    e1 = encode(d0)
    print(f"Re-encoded parameters {e1}")
    assert np.max(np.abs(np.array(e1)-np.array(e0)))<1e-6, f"Decode/encode do not match {e0=} {e1=}"
    argnum = len(e0)
    print(f"Number of arguments {argnum}")

    if ansatz is not None and ansatz_fn is not None:
        raise Exception("Only one of parameters `ansatz` or `ansatz_fn` can be given.")
    if ansatz is not None:
        a = ansatz
        a.xy = xy
        a.system = system
    elif ansatz_fn is not None:
        a = (Thiele if thiele else CDyn)(ansatz_fn, computer=comp, args=[xy]+save_state(p0))
    else:
        raise Exception("An ansatz should be provided in either `ansatz` of `ansatz_fn` arguments.")

    time1, hist, energy = run_thiele_dynamics(
        state=e0, ansatz=a, comp=comp,
        simulation_period=np.max(time), 
        dt=dt, 
        report_time=time[1]-time[0]-dt,
        decode=decode,
        )
    
    res = {
        'system': system.__dict__, 
        'time': time1,
        f'hist_{name}': hist, 
        f'energy_{name}': energy,
        }
#     for k, v in res.items():
#         print(f"{k}: {v.shape}")
    
    np.savez(f'{filename}_{name}.npz', **res)    
    return hist[-1]

#########################################################################################################

def run_analytic_dynamics(
        p0, ansatz:Ansatz, 
        simulation_period=0.1, dt=0.0002, report_time=0.02,
        filename = None, name='analytic', 
        ):

    a = ansatz
    # Initial state
    t = 0.
    last_report = t
    
    # Init data collection
    p = np.asarray(p0)
    hist = [list(p)]    
    energy = [a.energy(p)]
    time = [t]

    # Init graphics
    with Progress(transient=True) as progress:
        task = progress.add_task("Working", total=simulation_period)

        try:
            while t<=simulation_period:
                p = a.time_increment(p, dt=dt)

                t += dt
                if t>last_report+report_time:
                    last_report = t

                    # n = a(state)
                    # e = comp.energy(n)

                    e = a.energy(p)

                    progress.update(task, completed=t)

                    time.append( t )
                    hist.append( list(p) )
                    energy.append( e )
        except KeyboardInterrupt:
            pass

    time, hist, energy = np.asarray(time), np.asarray(hist), np.asarray(energy)

    if filename is not None:
        res = {
            'system': a.system.__dict__, 
            'time': time,
            f'hist_{name}': hist, 
            f'energy_{name}': energy,
            }
    #     for k, v in res.items():
    #         print(f"{k}: {v.shape}")
        
        np.savez(f'{filename}_{name}.npz', **res)    

    return np.asarray(time), np.asarray(hist), np.asarray(energy)

