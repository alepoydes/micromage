import numpy as np
import numpy.testing as npt
import jax.numpy as jnp

from .cfg import dtype

#####################################################################################
# Auxilliary

def take(a, idx, axis):
    return jnp.moveaxis(jnp.moveaxis(a,axis,0)[idx],0,axis)

def pack3(n,axis=-3):
    """
    Transform array of values at lattice points to values on elements.
    Input shape 3,..,2*sx+1/0,:,:.
    Output shape 3,...,sx,:,;,3.
    If applied three times, transform array of shape 3,2*sx+1/0,2*sy+1/0,2*sz+1/0 
    to shape 3,sx,sy,sz,3,3,3. 
    Input size encodes boundary conditions:
        even - periodic, odd - free.
    """
    sx = n.shape[axis]
    if sx%2==1:
        return jnp.stack((
            take(n,slice(None,-1,2),axis=axis),
            take(n,slice(1,None,2),axis=axis),
            take(n,slice(2,None,2),axis=axis),        
        ), axis=-1)
    else:
        a = take(n,slice(None,None,2),axis=axis)
        return jnp.stack((
            a,
            take(n,slice(1,None,2),axis=axis),
            jnp.roll(a,-1,axis=axis),     
        ), axis=-1)


def pack2(n, axis=-3):
    """
    Transform array of values at lattice points to values on elements.
    Input shape 3,2*sx+1/0,2*sy+1/0,2*sz+1/0.
    Output shape 3,sx,sy,sz,2,2,2.
    Input size encodes boundary conditions:
        even - periodic, odd - free.
    """
    sx = n.shape[axis]
    if sx%2==1:
        return jnp.stack((
            take(n, slice(None,-1), axis=axis),
            take(n, slice(1,None), axis=axis),
        ), axis=-1)
    else:
        return jnp.stack((
            n,
            jnp.roll(n,-1,axis=axis),     
        ), axis=-1)

def unpack3_mean(n,sx,axise,axis):
    """
    Do inverse operation to `pack3`.
    Some values are defined multiple times on different elements.
    In the case mean value is returned.
    `axis` defines axis of `n` indexing nodes inside the cell,
        corresponding elements are numerated by `axis-2`.
    `sz` defines size of the result and boundary conditions:
        even - periodic, odd - free.
    """
    n = jnp.moveaxis(n, axise, 0)
    n = jnp.moveaxis(n, axis, -1)    
    # Check dimensions
    rx = n.shape[0]
    assert n.shape[-1]==3
    assert sx//2==rx
    # Allocate result.
    n1 = jnp.empty((sx,)+n.shape[1:-1], dtype=n.dtype)
    # Copy value.
    if sx%2==1: # Free BC.
        n1 = n1.at[1::2].set( n[...,1] )
        n1 = n1.at[2:-1:2].set( (n[1:,...,0]+n[:-1,...,2])/2 )
        n1 = n1.at[0].set( n[0,...,0] )
        n1 = n1.at[-1].set( n[-2,...,2] )
    else: # Periodic BC
        n1 = n1.at[1::2].set( n[...,1] )
        n1 = n1.at[::2].set( (n[...,0]+jnp.roll(n[...,2],1,axis=0))/2 )
    return jnp.moveaxis(n1, 0, axise)
   


TRAP_P = np.array([-1,1], dtype=dtype)
TRAP_W = np.array([1,1], dtype=dtype)
GL2_P = np.array([-np.sqrt(1/3),np.sqrt(1/3)], dtype=dtype)
GL2_W = np.array([1,1], dtype=dtype)

SIMPSON_P = np.array([-1,0,1], dtype=dtype)
SIMPSON_W = np.array([1/3,4/3,1/3], dtype=dtype)
GL3_P = np.array([-np.sqrt(3/5),0,np.sqrt(3/5)], dtype=dtype)
GL3_W = np.array([5/9,8/9,5/9], dtype=dtype)
    
def intp3(n, x, y, axis):
    """
    Interpolate valus of the array `n` defined at points `x` to points `y`.
    Arguments:
        `n` - multidimensional array,
        `axis` -  axis of `n` along which the interpolation is done.
        `x` - 1d array of coordinates of points of `n`.
        `y` - 1d array of coordinates of points of the output.
    """
    n = jnp.array(n, dtype=dtype)
    x = jnp.array(x, dtype=dtype)
    y = jnp.array(y, dtype=dtype)
    assert x.ndim == 1 and y.ndim == 1    
    assert n.shape[axis]==x.shape[0]==3
    
    n = jnp.moveaxis(n, axis, 0)[...,None]
    
    a = (y-x[1])*(y-x[2])/(x[0]-x[1])/(x[0]-x[2])
    b = (y-x[0])*(y-x[2])/(x[1]-x[0])/(x[1]-x[2])
    c = (y-x[0])*(y-x[1])/(x[2]-x[0])/(x[2]-x[1])
    
    n1 = a*n[0]+b*n[1]+c*n[2]
    
    return jnp.moveaxis(n1, -1, axis) 
    
npt.assert_allclose( 
    intp3([[2,3,4],[-2,-1,0]],x=[-1,0,1],y=[0,1,-1],axis=-1), 
    [[3,4,2],[-1,0,-2]] 
)

def intp2(n, x, y, axis):
    """
    Interpolate valus of the array `n` defined at points `x` to points `y`.
    Arguments:
        `n` - multidimensional array,
        `axis` -  axis of `n` along which the interpolation is done.
        `x` - 1d array of coordinates of points of `n`.
        `y` - 1d array of coordinates of points of the output.
    """
    n = jnp.array(n, dtype=dtype)
    x = jnp.array(x, dtype=dtype)
    y = jnp.array(y, dtype=dtype)
    assert x.ndim == 1 and y.ndim == 1    
    assert n.shape[axis]==x.shape[0]==2
    
    n = jnp.moveaxis(n, axis, 0)[...,None]
    
    a = (y-x[1])/(x[0]-x[1])
    b = (y-x[0])/(x[1]-x[0])
    
    n1 = a*n[0]+b*n[1]
    
    return jnp.moveaxis(n1, -1, axis) 
    
npt.assert_allclose( 
    intp2([[2,3],[-2,-1]],x=[-1,1],y=[1,-1],axis=-1), 
    [[3,2],[-1,-2]] 
)

def intp3_s(n, x, y, axis):
    """
    An analogue of `intp3`, but `y` is a list/array of local coordinates, 
    and `n` is a list/array of cells. 
    Returned value is a list/array of interpolated values, one for each cell.
    """
    n = jnp.array(n, dtype=dtype)
    x = jnp.array(x, dtype=dtype)
    y = jnp.array(y, dtype=dtype)
    assert x.ndim == 1 and y.ndim == 1
    assert n.shape[axis]==x.shape[0]==3
    assert n.shape[0]==y.shape[0]

    n = jnp.moveaxis(n, axis, 0)
    n = jnp.moveaxis(n, 1, -1)

    a = (y-x[1])*(y-x[2])/(x[0]-x[1])/(x[0]-x[2])
    b = (y-x[0])*(y-x[2])/(x[1]-x[0])/(x[1]-x[2])
    c = (y-x[0])*(y-x[1])/(x[2]-x[0])/(x[2]-x[1])
    
    n1 = a*n[0]+b*n[1]+c*n[2]
    
    return jnp.moveaxis(n1, -1, 0)

def intp2_s(n, x, y, axis):
    """
    An analogue of `intp3`, but `y` is a list/array of local coordinates, 
    and `n` is a list/array of cells. 
    Returned value is a list/array of interpolated values, one for each cell.
    """
    n = jnp.array(n, dtype=dtype)
    x = jnp.array(x, dtype=dtype)
    y = jnp.array(y, dtype=dtype)
    assert x.ndim == 1 and y.ndim == 1
    assert n.shape[axis]==x.shape[0]==2
    assert n.shape[0]==y.shape[0]

    n = jnp.moveaxis(n, axis, 0)
    n = jnp.moveaxis(n, 1, -1)

    a = (y-x[1])/(x[0]-x[1])
    b = (y-x[0])/(x[1]-x[0])
    
    n1 = a*n[0]+b*n[1]
    
    return jnp.moveaxis(n1, -1, 0)


def quad(n, w, axis):
    """
    Compute quadrature along the `axis` of the array `n` with weights `w`.
    """
    n = jnp.array(n, dtype=dtype)
    w = jnp.array(w, dtype=dtype)    
    assert w.ndim==1
    assert n.shape[axis]==w.shape[0]    

    n = jnp.moveaxis(n, axis, -1)
    
    s = jnp.sum(n*w,axis=-1)
    
    return jnp.moveaxis(s[...,None],-1, axis)

npt.assert_allclose( 
    quad((lambda x: x**2+x-1)(SIMPSON_P), SIMPSON_W, axis=0), 
    [-4/3] 
)

npt.assert_allclose( 
    quad((lambda x: x**4-x**3+x**2-x+1)(GL3_P), GL3_W, axis=0), 
    [46/15] 
)


def diff3(n, x, y, axis):
    """
    Compute the finite difference for the array `n` along the `axis`
    at the points `y` assuming `n` is defined at points `x`.
    """
    n = jnp.array(n, dtype=dtype)
    x = jnp.array(x, dtype=dtype)
    y = jnp.array(y, dtype=dtype)
    assert x.ndim == 1 and y.ndim == 1    
    # assert n.shape[axis]==x.shape[0]==3
    
    n = jnp.moveaxis(n, axis, 0)[...,None]

    # a = (y-x[1])*(y-x[2])/(x[0]-x[1])/(x[0]-x[2])
    da = (2*y-x[1]-x[2])/(x[0]-x[1])/(x[0]-x[2])
    # b = (y-x[0])*(y-x[2])/(x[1]-x[0])/(x[1]-x[2])
    db = (2*y-x[0]-x[2])/(x[1]-x[0])/(x[1]-x[2])    
    # c = (y-x[0])*(y-x[1])/(x[2]-x[0])/(x[2]-x[1])
    dc = (2*y-x[0]-x[1])/(x[2]-x[0])/(x[2]-x[1])    
    
    d = da*n[0]+db*n[1]+dc*n[2]
        
    return jnp.moveaxis(d, -1, axis) 

npt.assert_allclose( 
    diff3((lambda x: x**2+x-1)(SIMPSON_P), x=SIMPSON_P, y=GL3_P, axis=0),
    (lambda x: 2*x+1)(GL3_P)
)

def diff2(n, x, y, axis):
    """
    Compute the finite difference for the array `n` along the `axis`
    at the points `y` assuming `n` is defined at points `x`.
    """
    n = jnp.array(n, dtype=dtype)
    x = jnp.array(x, dtype=dtype)
    y = jnp.array(y, dtype=dtype)
    assert x.ndim == 1 and y.ndim == 1    
    assert n.shape[axis]==x.shape[0]==2
    
    n = jnp.moveaxis(n, axis, 0)[...,None]

    # a = (y-x[1])/(x[0]-x[1])
    da = 1/(x[0]-x[1]) + 0*y
    # b = (y-x[0])/(x[1]-x[0])
    db = 1/(x[1]-x[0]) + 0*y
    
    d = da*n[0]+db*n[1]
        
    return jnp.moveaxis(d, -1, axis) 

npt.assert_allclose( 
    diff2((lambda x: 2*x-3)(TRAP_P), x=TRAP_P, y=GL2_P, axis=0),
    (lambda x: 2+0*x)(GL2_P)
)

def quad3(n):
    """
    Compute integrals over elements assuming function is defined at `GL3_P` points.
    """
    return jnp.sum(quad(quad(quad(n,w=GL3_W,axis=-1),w=GL3_W,axis=-2),w=GL3_W,axis=-3))

def quad2(n):
    """
    Compute integrals over elements assuming function is defined at `GL2_P` points.
    """
    return jnp.sum(quad(quad(quad(n,w=GL2_W,axis=-1),w=GL2_W,axis=-2),w=GL2_W,axis=-3))

def space_derivative(n, axis=-1):
    if axis<0: axis += n.ndim
    sx = n.shape[axis]
    n = pack3(n, axis=axis)
    dn = diff3(n, SIMPSON_P, SIMPSON_P, axis=-1)
    return unpack3_mean(dn, sx=sx, axise=axis, axis=-1)
