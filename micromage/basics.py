import jax.numpy as jnp
import jax
import jax_dataclasses as jdc

import numpy as np
# from dataclasses import dataclass, field
from functools import partial

from .cfg import dtype
from .elem import (quad, diff2, diff3, intp2, intp3, pack2, pack3, intp3_s, intp2_s,
    TRAP_P, TRAP_W, GL2_P, GL2_W,
    SIMPSON_P, SIMPSON_W, GL3_P, GL3_W,
    space_derivative,
)

#####################################################################################
# System parameters

@jdc.pytree_dataclass
class System:
    # Energy related parameters.
    A: float = 1 # Intralayer Heisenberg exchange.
    J: float = 1 # Interlayer Heisenberg exchange.    
    Db: float = 0 # Intralayer DMI: bulk
    Di: float = 0 # Intralayer DMI: interfacial 
    H: float = 0 # External magnetic field along z-axis.
    K: float = 0 # Anisotropy along z-axis (>0 = easy axis).
    Lx: float = 1 # Length of domain size.
    Ly: float = 1 # Width of domain size.
    
    # Dynamics related parameters.
    Ms: float = 1 # Saturation magnetization.
    gamma: float = 1 # Gyromagnetic ratio.
    eta: float = 0.2 # Damping.
    beta: float = 0. # Current dissipation factor.
    jx: float = 0. # Current x projection.
    jy: float = 0. # Current y projection.
    
    # Impurities
    imp_x: np.ndarray = jdc.field(default_factory=lambda: np.array([],dtype=dtype)) # Impurity x-position
    imp_y: np.ndarray = jdc.field(default_factory=lambda: np.array([],dtype=dtype)) # Impurity y-position
    # imp_z: np.ndarray = jdc.field(default_factory=lambda: np.array([],dtype=np.int32)) # Impurity layer
    imp_K: np.ndarray = jdc.field(default_factory=lambda: np.array([],dtype=dtype)) # Impurity strength (anisotropy variation)
    
    @property
    def pitch(self):
        """
        Helix pitch.
        """
        D = np.sqrt(self.Di**2+self.Db**2)
        return 4*np.pi*self.A/D

    def  __post_init__(self):
        assert self.imp_x.ndim==1
        assert self.imp_x.shape==self.imp_y.shape==self.imp_K.shape

#####################################################################################
# Auxiliary

def cross(n1, n2):
    a1 = jnp.moveaxis(n1, 0, -1)
    a2 = jnp.moveaxis(n2, 0, -1)
    a = jnp.cross(a1, a2)
    return jnp.moveaxis(a, -1, 0)

def normalize(n):
    return n/jnp.sqrt(jnp.sum(n**2,axis=0))[None]

@jax.jit
def grad_normalize(dn, n):
    dn = dn-jnp.sum(n*dn,axis=0)[None]*n
    return dn

def random(sx, sy=None, sz=None):
    if sy is None: sy=sx
    if sz is None: sz=sx    
    n = np.random.randn(3,sx,sy,sz).astype(dtype)
    return normalize(n)

def fm(sx, sy=None, sz=None):
    if sy is None: sy=sx
    if sz is None: sz=sx    
    n = np.zeros((3,sx,sy,sz), dtype=dtype)
    n[2] = 1
    return n

def lagrange_mult(de, n):
    return jnp.sum(de*n,axis=0)

###############################################################################################

def quad_xy3(n):
    return jnp.sum(quad(quad(n,w=GL3_W,axis=-1),w=GL3_W,axis=-2),axis=(-1,-2))

def quad_xy2(n):
    return jnp.sum(quad(quad(n,w=GL2_W,axis=-1),w=GL2_W,axis=-2),axis=(-1,-2))


@jax.jit
def inner3(n1, n2, s:System):
    """
    Inner product in L2 = integral over x and y and sum over z and projection of magnetization.
    """
    n1 = pack3(pack3(n1))
    n2 = pack3(pack3(n2))    

    sx, sy, sz = n1.shape[1:4]
    assert n1.shape==(3,sx,sy,sz,3,3), f"Wrong shape {n1.shape}"
    assert n2.shape==(3,sx,sy,sz,3,3), f"Incompatible shapes {n1.shape} {n2.shape}"
    
    ax = s.Lx/sx/2 # Grid step along x-axis.
    ay = s.Ly/sy/2 # Grid step along y-axis.
    
    n1 = intp3(n1, x=SIMPSON_P, y=GL3_P, axis=-2)
    n1 = intp3(n1, x=SIMPSON_P, y=GL3_P, axis=-1)
    
    n2 = intp3(n2, x=SIMPSON_P, y=GL3_P, axis=-2)
    n2 = intp3(n2, x=SIMPSON_P, y=GL3_P, axis=-1)
    
    p = quad_xy3(n1*n2)
    
    return jnp.sum(p)*ax*ay
    
@jax.jit
def inner2(n1, n2, s:System):
    """
    Inner product in L2 = integral over x and y and sum over z and projection of magnetization.
    """
    n1 = pack2(pack2(n1))
    n2 = pack2(pack2(n2))    

    sx, sy, sz = n1.shape[1:4]
    assert n1.shape==(3,sx,sy,sz,2,2), f"Wrong shape {n1.shape}"
    assert n2.shape==(3,sx,sy,sz,2,2), f"Incompatible shapes {n1.shape} {n2.shape}"
    
    ax = s.Lx/sx/2 # Grid step along x-axis.
    ay = s.Ly/sy/2 # Grid step along y-axis.
    
    n1 = intp2(n1, x=TRAP_P, y=GL2_P, axis=-2)
    n1 = intp2(n1, x=TRAP_P, y=GL2_P, axis=-1)
    
    n2 = intp2(n2, x=TRAP_P, y=GL2_P, axis=-2)
    n2 = intp2(n2, x=TRAP_P, y=GL2_P, axis=-1)
    
    p = quad_xy2(n1*n2)
    
    return jnp.sum(p)*ax*ay


################################################################################################

@partial(jax.jit, static_argnums=[0,1])
def coordinates_xy(sx, sy, system):
    ex, ey = sx//2, sy//2
    ax, ay = system.Lx/ex/2, system.Ly/ey/2
    xx, yy = jnp.arange(sx)*ax, jnp.arange(sy)*ay
    return jnp.stack(jnp.meshgrid(xx, yy, indexing='ij'), axis=0).astype(dtype)

################################################################################################
# Micromagnetic energy.
# First order.

@jax.jit
def energy1(ns, s:System):
    """
    The magnetization field should be given at `SIMPSON_P` points.
    """
    ns = pack2(pack2(ns))
    
    sx, sy, sz = ns.shape[1:4]
    assert ns.shape==(3,sx,sy,sz,2,2), f"Wrong shape {ns.shape}"
    ax = s.Lx/sx/2 # Grid step along x-axis.
    ay = s.Ly/sy/2 # Grid step along y-axis.
    
    n = intp2(ns, x=TRAP_P, y=GL2_P, axis=-2)
    n = intp2(n,  x=TRAP_P, y=GL2_P, axis=-1)
    
    ndx = diff2(n, x=GL2_P, y=GL2_P, axis=-2)/ax
    ndy = diff2(n, x=GL2_P, y=GL2_P, axis=-1)/ay

    rotx, roty, rotz = ndy[2], -ndx[2], ndx[1]-ndy[0]
    divn = ndx[0]+ndy[1]

    e_heisenberg_ra = s.A*jnp.sum(
        quad_xy2(jnp.sum(ndx**2,axis=0))+
        quad_xy2(jnp.sum(ndy**2,axis=0))
    )
    
    e_heisenberg_er = -s.J*jnp.sum(
        quad_xy2(jnp.sum(n[:,:,:,1:]*n[:,:,:,:-1],axis=0))
    ) # S[z=j]*S[z=j-1]
    
    e_dm_b = s.Db*jnp.sum(
        quad_xy2(n[0]*rotx)+
        quad_xy2(n[1]*roty)+
        quad_xy2(n[2]*rotz)
    )

    e_dm_i = s.Di*jnp.sum(
        quad_xy2(n[2]*divn)
        -quad_xy2(n[0]*ndx[2])
        -quad_xy2(n[1]*ndy[2])
    )
    
    e_ani = -s.K*jnp.sum(quad_xy2(n[2]**2-1))
    
    e_zeeman = -s.H*jnp.sum(quad_xy2(n[2]-1))

    e_imp = jnp.sum(s.imp_K[:,None] * interpolate2(ns, s.imp_x/(2*ax), s.imp_y/(2*ay))[2]**2 )
        
    return (e_heisenberg_ra + e_heisenberg_er + e_dm_i + e_dm_b + e_zeeman + e_ani) * (ax*ay) + e_imp


################################################################################################
# Micromagnetic energy.
# Second order.

@jax.jit
def energy2(ns, s:System):
    """
    The magnetization field should be given at `SIMPSON_P` points.
    """
    ns = pack3(pack3(ns))
    
    sx, sy, sz = ns.shape[1:4]
    assert ns.shape==(3,sx,sy,sz,3,3), f"Wrong shape {ns.shape}"
    ax = s.Lx/sx/2 # Grid step along x-axis.
    ay = s.Ly/sy/2 # Grid step along y-axis.
    
    n = intp3(ns, x=SIMPSON_P, y=GL3_P, axis=-2)
    n = intp3(n,  x=SIMPSON_P, y=GL3_P, axis=-1)
    
    ndx = diff3(n, x=GL3_P, y=GL3_P, axis=-2)/ax
    ndy = diff3(n, x=GL3_P, y=GL3_P, axis=-1)/ay

    rotx, roty, rotz = ndy[2], -ndx[2], ndx[1]-ndy[0]
    divn = ndx[0]+ndy[1]
    
    e_heisenberg_ra = s.A*jnp.sum(
        quad_xy3(jnp.sum(ndx**2,axis=0))+
        quad_xy3(jnp.sum(ndy**2,axis=0))
    )
    
    e_heisenberg_er = -s.J*jnp.sum(
        quad_xy3(jnp.sum(n[:,:,:,1:]*n[:,:,:,:-1],axis=0))
    ) # S[z=j]*S[z=j-1]
    
    e_dm_b = s.Db*jnp.sum(
        quad_xy3(n[0]*rotx)+
        quad_xy3(n[1]*roty)+
        quad_xy3(n[2]*rotz)
    )

    e_dm_i = s.Di*jnp.sum(
        quad_xy3(n[2]*divn)
        -quad_xy3(n[0]*ndx[2])
        -quad_xy3(n[1]*ndy[2])
    )
    
    e_ani = -s.K*jnp.sum(quad_xy3(n[2]**2-1))
    
    e_zeeman = -s.H*jnp.sum(quad_xy3(n[2]-1))

    e_imp = jnp.sum(s.imp_K[:,None] * interpolate3(ns, s.imp_x/(2*ax), s.imp_y/(2*ay))[2]**2 )
        
    return (e_heisenberg_ra + e_heisenberg_er + e_dm_i + e_dm_b + e_zeeman + e_ani) * (ax*ay) + e_imp

#################################################################################

@jax.jit
def time_derivative_wo_cross(n, dn, s:System):
    sx, sy = n.shape[1:3]
    ex, ey = sx//2, sy//2
    ax, ay = s.Lx/ex/2, s.Ly/ey/2

    heff = dn*(s.gamma/(s.Ms*ax*ay)) 
    dnx, dny = space_derivative(n, axis=-3)/ax, space_derivative(n, axis=-2)/ay
    jt = s.jx*dnx + s.jy*dny
    taun = -cross(n, jt)-s.beta*jt
    f = taun + heff
    alpha = s.gamma*s.eta*s.Ms
    # dn/dt = n x f + alpha n x dn/dt,
    # n x dn/dt = n x n x f + alpha n x n x dn/dt,
    # dn/dt = n x f + alpha n x n x (f + alpha dn/dt),
    # (1+alpha**2) dn/dt = n x f + alpha n x n x f = n x (f + alpha n x f).
    return (f + alpha * cross(n, f))/(1+alpha**2)


@jax.jit
def semi_imp(n, dt, f):
    # dn/dt = n x f
    # n1 = n(t+dt), n = n(t),
    # (n1-n)/dt = (n1+n)/2 x f,
    # n1-n = dt/2 (n1+n) x f,
    # n1 - dt/2 n1 x f = n + dt/2 n x f,
    # n1 = (1+dt/2 f x)^{-1}(1-dt/2 f x) n.
    x = dt/2 * f[0]
    y = dt/2 * f[1]
    z = dt/2 * f[2]
    x2, y2, z2 = x**2, y**2, z**2
    d = x2+y2+z2+1
    rx = (1+x2-y2-z2)*n[0] +    2*(x*y+z)*n[1] +    2*(x*z-y)*n[2]
    ry =    2*(x*y-z)*n[0] + (1-x2+y2-z2)*n[1] +    2*(y*z+x)*n[2]
    rz =    2*(x*z+y)*n[0] +    2*(y*z-x)*n[1] + (1-x2-y2+z2)*n[2]
    return jnp.stack([rx,ry,rz],axis=0)/d[None]

@jax.jit
def semi_imp_trig(n, dt, f):
    # Force absolute value.
    l2 = jnp.sum(f**2, axis=0, keepdims=True)
    l = jnp.sqrt(l2)
    # Find basis.
    e = f/jnp.maximum(l,1e-16) # Axis of rotation
    theta = dt*l
    c, s = jnp.cos(theta), jnp.sin(theta)
    return c*n + ((1-c)*jnp.sum(n*e, axis=0))*e+s*jnp.cross(n, e, axis=0)


##################################################################################

def interpolate3(ns, xn, yn): 
    # ns.shape = (3, sx, sy, sz, 3, 3)
    xi, yi = jnp.floor(xn).astype(np.int32), jnp.floor(yn).astype(np.int32)
    xl, yl = (xn-xi)*2-1, (yn-yi)*2-1
    # print(f"{ns.shape=} {xn=} {yn=}")
    # print(f"{xi=} {yi=} {xl=} {yl=}")
    n = ns[:,xi,yi] # n.shape = (3, N, sz, 3, 3)
    # print(f"{n.shape=}")
    assert n.shape[1]==yn.shape[0]
    n = jnp.swapaxes(n, 0, 1) # ns.shape = (N, 3, sz, 3, 3)
    n = intp3_s(n, x=SIMPSON_P, y=yl, axis=-1) # ns.shape = (N, 3, sz, 3)
    n = intp3_s(n, x=SIMPSON_P, y=xl, axis=-1) # ns.shape = (N, 3, sz)
    return jnp.swapaxes(n, 0, 1)


def interpolate2(ns, xn, yn): 
    # ns.shape = (3, sx, sy, sz, 2, 2)
    xi, yi = jnp.floor(xn).astype(np.int32), jnp.floor(yn).astype(np.int32)
    xl, yl = (xn-xi)*2-1, (yn-yi)*2-1
    # print(f"{ns.shape=} {xn=} {yn=}")
    # print(f"{xi=} {yi=} {xl=} {yl=}")
    n = ns[:,xi,yi] # n.shape = (3, N, sz, 3, 3)
    # print(f"{n.shape=}")
    assert n.shape[1]==yn.shape[0]
    n = jnp.swapaxes(n, 0, 1) # ns.shape = (N, 3, sz, 2, 2)
    n = intp2_s(n, x=TRAP_P, y=yl, axis=-1) # ns.shape = (N, 3, sz, 2)
    n = intp2_s(n, x=TRAP_P, y=xl, axis=-1) # ns.shape = (N, 3, sz)
    return jnp.swapaxes(n, 0, 1)

##################################################################################

class Computer:
    def __init__(self, system: System, energy):
        self.system = system
        self._energy = jax.jit(energy)
        self._d_energy = jax.jit(jax.grad(self._energy, 0))
        self._v_d_energy = jax.jit(jax.value_and_grad(self._energy, 0))
        
    def hessian(self, dn, l):
        s = jdc.replace(self.system, H=0.)
        return self._d_energy(dn,s)-l[None]*dn
        
    def energy(self, ns):
        return self._energy(ns, self.system)
    
    def d_energy(self, ns):
        return self._d_energy(ns, self.system)

    def v_d_energy(self, ns):
        return self._v_d_energy(ns, self.system)
       
    def time_derivative(self, ns):
        dn = self._d_energy(ns, self.system)
        return cross(ns, time_derivative_wo_cross(ns, dn, self.system))        
    
    def time_increment_old(self, ns, dt, rep=2):
        n = ns
        dn0 = None
        for I in range(rep):
            dn = self._d_energy(n, self.system)
            if dn0 is None: 
                dn0=dn
                dn_m = (dn+dn0)/2
            else:
                dn_m = dn
            f = time_derivative_wo_cross(n, dn_m, self.system)
            n = semi_imp(ns, dt, f)
        return n
    
    def time_increment_mid_trig(self, ns, dt):
        # Midpoint Euler.
        dn0 = self._d_energy(ns, self.system)
        f0 = time_derivative_wo_cross(ns, dn0, self.system)
        n1 = semi_imp_trig(ns, dt/2, f0)
        dn1 = self._d_energy(n1, self.system)
        f1 = time_derivative_wo_cross(n1, dn1, self.system)
        n2 = semi_imp_trig(ns, dt, f1)
        return n2

    def time_increment(self, ns, dt):
        return self.time_increment_rk4(ns, dt)

    def time_increment_rk4(self, ns, dt):
        # Runge-Kutta 4ord.
        dn0 = self._d_energy(ns, self.system)
        f0 = time_derivative_wo_cross(ns, dn0, self.system)
        n1 = semi_imp(ns, dt/2, f0)
        dn1 = self._d_energy(n1, self.system)
        f1 = time_derivative_wo_cross(n1, dn1, self.system)
        n2 = semi_imp(ns, dt/2, f1)
        dn2 = self._d_energy(n2, self.system)
        f2 = time_derivative_wo_cross(n2, dn2, self.system)
        n3 = semi_imp(ns, dt, f2)
        dn3 = self._d_energy(n3, self.system)
        f3 = time_derivative_wo_cross(n3, dn3, self.system)
        f = (f0+2*(f1+f2)+f3)/6
        n = semi_imp(ns, dt, f)
        return n


    def time_increment_rk4_trig(self, ns, dt):
        # Runge-Kutta 4ord.
        dn0 = self._d_energy(ns, self.system)
        f0 = time_derivative_wo_cross(ns, dn0, self.system)
        n1 = semi_imp_trig(ns, dt/2, f0)
        dn1 = self._d_energy(n1, self.system)
        f1 = time_derivative_wo_cross(n1, dn1, self.system)
        n2 = semi_imp_trig(ns, dt/2, f1)
        dn2 = self._d_energy(n2, self.system)
        f2 = time_derivative_wo_cross(n2, dn2, self.system)
        n3 = semi_imp_trig(ns, dt, f2)
        dn3 = self._d_energy(n3, self.system)
        f3 = time_derivative_wo_cross(n3, dn3, self.system)
        f = (f0+2*(f1+f2)+f3)/6
        n = semi_imp_trig(ns, dt, f)
        return n
        
    def coordinates_for(self, n):
        return self.coordinates(*n.shape[1:3])
    
    def coordinates(self, sx, sy):
        return coordinates_xy(sx, sy, self.system)

    def lattice_constants(self, n):
        sx, sy = n.shape[1:3]
        ex, ey = sx//2, sy//2
        s = self.system
        ax, ay = s.Lx/ex/2, s.Ly/ey/2
        return ax, ay

    def space_derivative(self, n):
        # s = self.system    
        ax, ay = self.lattice_constants(n)
        return space_derivative(n, axis=-3)/ax, space_derivative(n, axis=-2)/ay

    
class Computer2(Computer):
    def __init__(self, system: System):
        super().__init__(system=system, energy=energy2)
    
    def inner(self, n1, n2):
        return inner3(n1, n2, self.system)


class Computer1(Computer):
    def __init__(self, system: System):
        super().__init__(system=system, energy=energy1)

    def inner(self, n1, n2):
        return inner2(n1, n2, self.system)


##################################################################################

class Ansatz:
    def __init__(self, ansatz, computer: Computer, args=[]):
        assert isinstance(computer, Computer)
        self.computer = computer
        self.args = args
        self.ansatz = jax.jit(ansatz)
        self._jac = jax.jit(jax.jacfwd(self.ansatz,argnums=0))
        
    def __call__(self, params):
        return self.ansatz(params, *self.args)
    
    def jacobian(self, params):
        jac = jnp.asarray( self._jac(params, *self.args) )
        # print(f"{params=} {type(jac)=} {[type(t) for t in jac]=}")
        return jnp.moveaxis(jac,-1,0)
    
    def inner(self, a, b):
        return self.computer.inner(a,b)
        # return jnp.sum(a*b)

    def energy(self, params):
        return self.computer.energy(self(params))
    

class CDyn(Ansatz):
    def gram_matrix(self, jac):
        argnum = len(jac)
        mat = jnp.empty((argnum,)*2, dtype=jac[0].dtype)
        for n in range(argnum):
            for m in range(n+1):
                p = self.inner(jac[n], jac[m])
                mat = mat.at[n,m].set(p)
                mat = mat.at[m,n].set(p)
        return mat
    
    def jac_mul(self, dn, jac):
        return jnp.array(list(self.inner(dn,da) for da in jac))
        
    def projections_to_tangent(self, dn, jac):
        mat = self.gram_matrix(jac)
        y = self.jac_mul(dn, jac)
        x = jnp.linalg.lstsq(mat, y)[0]
        return x
    
    def residual(self, dn, jac, proj):
        assert len(jac)==len(proj)
        for da, p in zip(jac, proj):
            dn = dn - da*p
        return dn
    
    def time_derivative(self, pars, residual=False):
        n = self(pars)
        dn = self.computer.time_derivative(n)
        jac = self.jacobian(pars)
        proj = self.projections_to_tangent(dn, jac)
        if residual:
            res = self.residual(dn, jac, proj)
            return proj, res
        return proj
    
    def time_derivative_ort(self, pars):
        n = self(pars)
        dn = self.computer.time_derivative(n)
        # print(jnp.any(jnp.isnan(n)), jnp.any(jnp.isnan(dn)), jnp.any(jnp.isnan(da)), )
        dp = []
        for k in range(len(pars)):
            da = self.diff(pars, k)
            dnda = self.inner(dn,da)
            da2 = self.inner(da,da)
            # print(f"{k=} {dnda} {da2}")
            dp.append( dnda/da2 )
        return jnp.asarray(dp)    
    
    def time_increment(self, p, dt):
        # Runge-Kutta method.
        k1 = self.time_derivative(p)
        k2 = self.time_derivative(p+0.5*dt*k1)
        k3 = self.time_derivative(p+0.5*dt*k2)
        k4 = self.time_derivative(p+dt*k3)
        return p+dt/6*k1+dt/3*k2+dt/3*k3+dt/6*k4

###################################################################################################

class Thiele(Ansatz):
    # def __init__(self, ansatz, computer: Computer, args=[]):
    #     assert isinstance(computer, Computer)
    #     self.computer = computer
    #     self.args = args
    #     self.ansatz = jax.jit(ansatz)
    #     self._jac = jax.jit(jax.jacfwd(self.ansatz,argnums=0))
        
    # def __call__(self, params):
    #     return self.ansatz(params, *self.args)
    
    # def jacobian(self, params):
    #     return jnp.moveaxis(self._jac(params, *self.args),-1,0)
    
    # def inner(self, a, b):
    #     # return self.computer.inner(a,b)
    #     return jnp.sum(a*b)
    
    def equation(self, params):
        K = len(params) # Number of parameters
        ns = self(params)
        jac = self.jacobian(params)
        # print(f"{n.shape=} {jac.shape=}")
        # assert jac.shape[0]==K
        # assert jac.shape[1]==3
        # assert n.shape[0]==3
        # Compute matrices.
        A = jnp.empty((K,K), dtype=ns.dtype)
        B = jnp.empty((K,K), dtype=ns.dtype)
        for n in range(K):
            for m in range(n+1):
                p = self.inner(jac[n], jac[m])
                A = A.at[n,m].set(p)
                A = A.at[m,n].set(p)
                q = self.inner(jac[n],cross(ns,jac[m]))
                B = B.at[n,m].set(q)
                B = B.at[m,n].set(-q)
        # Compute effective field.
        grad = self.computer.d_energy(ns)
        dE = jnp.empty((K,), dtype=ns.dtype)
        for k in range(K):
            dE = dE.at[k].set( self.inner(grad, jac[k]) )
        ax, ay = self.computer.lattice_constants(ns)
        dE = dE / (ax*ay)
        # Compute current associated torques.
        dx, dy = self.computer.space_derivative(ns)
        jx = jnp.empty((K,), dtype=ns.dtype)
        jy = jnp.empty_like(jx)
        jx2 = jnp.empty_like(jx)
        jy2 = jnp.empty_like(jx)
        for k in range(K):
            jx = jx.at[k].set( self.inner(dx, jac[k]) )
            jy = jy.at[k].set( self.inner(dy, jac[k]) )
            jx2 = jx2.at[k].set( self.inner(cross(ns,dx), jac[k]) )
            jy2 = jy2.at[k].set( self.inner(cross(ns,dy), jac[k]) )
        return A,B,dE,jx,jy,jx2,jy2
    
    def time_derivative(self, params):
        s = self.computer.system
        A,B,dE,jx,jy,jx2,jy2 = self.equation(params)
        mat = (s.eta*s.Ms)*A + 1/s.gamma*B
        rhs = -(1/s.Ms)*dE + s.jx*jx2+s.jy*jy2 + s.jx*s.beta*jx+s.jy*s.beta*jy 
        # return jnp.linalg.lstsq(mat, rhs)[0]
        return np.linalg.lstsq(mat, rhs, rcond=None)[0]

    
    def time_increment(self, p, dt):
        # Runge-Kutta method.
        k1 = self.time_derivative(p)
        k2 = self.time_derivative(p+0.5*dt*k1)
        k3 = self.time_derivative(p+0.5*dt*k2)
        k4 = self.time_derivative(p+dt*k3)
        return p+dt/6*k1+dt/3*k2+dt/3*k3+dt/6*k4

        
        
        
