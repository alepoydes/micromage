import numpy as np
import matplotlib
import matplotlib.pyplot as plt

from .probe import center_of_magnetization, extract_profile, compute_radius, extract_chirality_profile


def vector_to_rgb(S:np.ndarray, axis=(0,0,1), invert=False, shift=0.81):
    C = np.empty_like(S)
    axis = np.array(axis)
    axis_norm = np.linalg.norm(axis)
    if axis_norm>0:
        axis = axis / axis_norm
    else:
        axis = np.array((0,0,1))
    C[0] = (np.arctan2(S[1], S[0])/np.pi+1)/2 + shift 
    C[0] -= np.floor(C[0])
    s = np.sum(np.moveaxis(S,0,-1)*axis, axis=-1)
    C[1 if invert else 2] = np.maximum(0, np.minimum(1+s, 1))
    C[2 if invert else 1] = np.maximum(0, np.minimum(1-s, 1))
    return matplotlib.colors.hsv_to_rgb(np.moveaxis(C,0,-1))


################################################################################3

def plot_state(n, system, fig=None, ax=None, im=None, figsize=(7,15)):
    """
    Function that show magnetic state in human comprehensible form.
    Probably you want to implement a custom one.
    Arguments:
     n - state of magnetic system = 3d vector field = array[x/y/z component of mangetization, x, y, z-coordinate]
     system - parameters of the magnetic system.
     fig - matplotlib figure. If None, a new figure is created.
     ax - list of matplotlib Axis objects, one for each z-layer.
     im - list of objected containing picture themselves.
     figsize - size of all the figure in inches.
    Return:
     fig, ax, im    
    """
    sz = n.shape[-1]
    rgb = vector_to_rgb(n)
    data = rgb.transpose((1,0,2,3))
    if im is None:
        if ax is None:
            fig, ax = plt.subplots(1,sz,figsize=figsize,facecolor='black',
                                   squeeze=False, sharex=True, sharey=True)
            ax = ax.flatten()
        im = []
        for k in range(sz):
            im.append( ax[k].imshow(data[:,:,k], origin='lower', extent=(0,system.Lx,0,system.Ly), interpolation='none') )
            ax[k].set_axis_off()
        fig.set_tight_layout(True)
    else:
        for k in range(sz):
            im[k].set_data(data[:,:,k])
    return fig, ax, im


##########################################################################33

def analyze_geometry(n, xy, figax=None, figsize=(15,5), var=0.1, time=None, chi0=0.):
    ns = n[...,0]
    center = center_of_magnetization(ns, xy)
    r, nz = extract_profile(ns, center, xy)
    
    rad, wdt = compute_radius(r, nz)

    r1, dphi, w = extract_chirality_profile(ns, center, xy, zthreshold=1)
    
    b = (1+nz)/(1-nz)
    rgb = vector_to_rgb(n[...,0]).transpose((1,0,2))
    
    if figax is None:
        fig, (ax, bx, dx) = plt.subplots(1,3,figsize=figsize)
        cx = bx.twinx()
        ma = ax.imshow(rgb, origin='lower', extent=(0,xy[0,-1,0],0,xy[1,0,-1])) 
        ax.set_axis_off()
        maxr = xy[0,-1,0]/2
        
        mb, = bx.plot(r, nz, ',b')
    #     bx.axhline(0, c='b')
    #     bx.axvline(rad, c='k')
    #     bx.axvline(rad+wdt/2, c='k', alpha=0.5)
    #     bx.axvline(rad-wdt/2, c='k', alpha=0.5)
        cx.axhline(1, c='k', ls=':')
        mc, = cx.semilogy(r, b, ',r')
        bx.set_ylim((-1.05,1.05))
        cx.set_ylim((1e-3,1e3))
        bx.set_xlim((0,maxr))
        bx.set_xlabel('Distance to center')
        bx.set_ylabel('Magnetization z-projection')

        md, = dx.plot(r1[w], dphi[w], ',k')
        dx.set_xlabel('Distance to center')
        dx.set_ylabel('Magnetization polar angle')
        if var is None:
            dx.set_ylim((-np.pi,np.pi))
        elif isinstance(var, tuple):
            dx.set_ylim(var)            
        else:
            dx.set_ylim((chi0-var,chi0+var))
        dx.set_xlim((0,maxr))
        
        txt = ax.text(0., 0., '', horizontalalignment='center',
             verticalalignment='center', transform=ax.transAxes)
        
        fig.set_tight_layout(True)    

    else:
        fig, ax, bx, cx, dx, ma, mb, mc, md, txt = figax
        ma.set_data(rgb)
        mb.set_data(r, nz)
        mc.set_data(r, b)
        md.set_data(r1[w], dphi[w])
    if time is not None:
        txt.set_text(f"T={time:.4f}")

    return fig, ax, bx, cx, dx, ma, mb, mc, md, txt


#################################################################################################


def plot_param_trajectory(hist, ax, color='k', label='none'):
    x, y, r, w, chir, skew, tors = hist.T
    dx, dy = x[1:]-x[:-1], y[1:]-y[:-1]
    cx, cy, cr = (x[1:]+x[:-1])/2, (y[1:]+y[:-1])/2, (r[1:]+r[:-1])/2
    dd = np.sqrt(dx**2+dy**2)+1e-12
    tx, ty = dx/dd, dy/dd
    nx, ny = -ty, tx
    
    ax.plot(x, y, c=color, ls='-', label=label)
    ax.plot(cx+nx*cr, cy+ny*cr, c=color, ls=':', label=None)
    ax.plot(cx-nx*cr, cy-ny*cr, c=color, ls=':', label=None)        

    
def plot_result(filename, figsize=(10,9), figax=None, suffix='llg', label='LLF', color='r'):
    data = np.load(f'{filename}', allow_pickle=True)
    time, hist, energy = (
        data['time'], data[f'hist_{suffix}'], data[f'energy_{suffix}'], 
    )

    if figax is None:
        fig, ax = plt.subplots(2, 2, figsize=figsize, squeeze=False)
        bx = ax[0][1].twinx()
        cx = ax[1][0].twinx()
        dx = ax[1][1].twinx()
    else:
        fig, ax, bx, cx, dx = figax
        
    plot_param_trajectory(hist, ax=ax[0][0], color=color, label=label)
    ax[0][0].legend()

    aa = ax[0][1]
    ln1 = aa.plot(time, hist[:,2], '-', color=color, label=f'{label} radius')
    ln2 = bx.plot(time, hist[:,3], '--', color=color, label=f'{label} width')
    aa.set_xlabel('Time')
    aa.set_ylabel('Radius')
    bx.set_ylabel('Width') 
    
    lns = ln1+ln2
    aa.legend(lns, [l.get_label() for l in lns])

    aa = ax[1][0]
    ln1 = aa.plot(time, hist[:,4], '-', color=color, label=f"{label} chirality")
    ln2 = cx.plot(time, hist[:,6], '--', color=color, label=f"{label} torsion")    
    aa.set_xlabel('Time')
    aa.set_ylabel('Chirality')
    cx.set_ylabel('Torsion')    

    lns = ln1+ln2
    aa.legend(lns, [l.get_label() for l in lns])
    
    aa = ax[1][1]
    ln1 = aa.plot(time, energy, '-', color=color, label=label)
    ln2 = dx.plot(time, hist[:,5], '--', color=color, label=f"{label} skew")
    aa.set_xlabel('Time')
    aa.set_ylabel('Energy') 
    dx.set_ylabel('Skew') 
    lns = ln1+ln2
    aa.legend(lns, [l.get_label() for l in lns])

    plt.tight_layout()
    
    print(f"Initial {label}    {hist[0]}")
    print(f"Final {label}      {hist[-1]}")
    
    return data, (fig, ax, bx, cx, dx)
