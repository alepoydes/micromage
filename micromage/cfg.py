import os
env = os.environ.get('MICROMAGE_DOUBLE')

if env is not None:
    from jax import config
    config.update("jax_enable_x64", True)

    import jax.numpy as jnp
    dtype = jnp.float64
else:
    import jax.numpy as jnp
    dtype = jnp.float32

