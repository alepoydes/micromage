import numpy as np
import jax.numpy as jnp
from rich.progress import Progress

from .basics import Computer, normalize, lagrange_mult, grad_normalize, semi_imp, semi_imp_trig, cross

#########################################################################################
# Constraints.

def constrain_translations(comp:Computer, de, n, tol=1e-16):
    dx, dy = comp.space_derivative(n)
    n_dx, n_dy = jnp.maximum(tol,jnp.sum(dx**2)), jnp.maximum(tol,jnp.sum(dy**2))
    x, y = jnp.sum(de*dx)/n_dx, jnp.sum(de*dy)/n_dy
    return de - x*dx - y*dy 

#########################################################################################
# Gradient descent.
    
def gradient_descent(n, comp:Computer, step=1e1, maxiter=1000, nrep=10):
    for it in range(maxiter):
        report = (nrep>=maxiter) or (nrep>0 and it%(maxiter//nrep)==0)
        if report:
            e, de = comp.v_d_energy(n)
        else:
            de = comp.d_energy(n)
        de = grad_normalize(de, n)
        if report:
            print(f"{it}: E {e} dE {jnp.linalg.norm(de.flatten(), ord=np.inf)}")
        n = n-step*de
        n = normalize(n)
    return n

#########################################################################################
# Nonlinear conjugate gradient.

def ncg(n, comp: Computer, maxiter=100, maxsubit=10, maxfails=10, nrep=10, maxstep=None, tol=0, 
        reset_every=50, constraints=None):
    with Progress(transient=True) as progress:
        task = progress.add_task("Minimizing", total=maxiter)
        
        acc = jnp.zeros_like(n)
        pred = None

        # Compute energy and gradient.
        e, de = comp.v_d_energy(n) 
        lag = lagrange_mult(de, n)    
        de = grad_normalize(de, n)
        if constraints is not None:
            de = constraints(comp, de, n)
        de_sq = jnp.sum(de**2)
        
        negcount = 0
        failedconv = 0
        msubit = 0
        for it in range(maxiter):
            progress.update(task, completed=it, refresh=True)
            if de_sq<=tol**2: 
                progress.console.print(f"[red]{it}:[/] E {e:.3f} dE {np.sqrt(de_sq):.2e} [red]Converged[/]") 
                return n
            
            report = True if nrep is None else it%(maxiter//nrep)==0
            # Estimate beta
            if it<1 or it%reset_every==0: 
                beta = 0.0
            else: 
                ## Polak Ribiere
                # b = jnp.sum(de0**2)
                # a = jnp.sum((de0-de)*de0)

                ## Fletcher–Reeves
                b = de0_sq 
                a = de_sq = jnp.sum(de**2)

                beta = max(a / b, 0.0)
            assert not jnp.isnan(beta)
            # Update direction
            acc = grad_normalize(beta*acc-de, n)
            # Estimate step size
            ve = jnp.sum(de*acc)
            vve = jnp.sum(acc*comp.hessian(acc, lag))
            step = -ve/vve 
            if step<0:
                step=-step
                negcount += 1
                # print(f"Negative step.")
            if maxstep is not None and step>maxstep: 
                step = maxstep

            # Make step.
            axis = cross(acc, n)
            for subit in range(maxsubit):
                # n1 = n + step*acc # Old variant, does not respect normalization.
                n1 = semi_imp(n, step, axis) # Good, but tends to constant for large steps.
                # n1 = semi_imp_trig(n, step, axis) # Slow, but can make arbitrary rotation.
                # Compute energy and gradient.
                e1, de1 = comp.v_d_energy(n1)
                lag1 = lagrange_mult(de1, n)            
                de1 = grad_normalize(de1, n)
                if constraints is not None:
                    de1 = constraints(comp, de1, n1)
                pred = e + step*ve + vve*step**2/2
                err = (e1-pred)/step**2
                # print(f"{it}:{subit}: E {e} -> E {e1} step {step} PE {err}")
                # Accept if energy decreased.
                if e1<e:
                    break
                # Compute new step size.
                step /= 2
            msubit = max(subit,msubit)
            if e1>=e:
                failedconv += 1
                if failedconv>maxfails:
                    print("Failed to converge")
                    break

            # Report progress
            if report:
                denorm = jnp.linalg.norm(de.flatten(),ord=np.inf)
                progress.console.print(f"[red]{it}:[/] E {e:.3f} dE {np.sqrt(de_sq):.2e} {denorm:.2e} Step {step:.2e} Beta {beta:.2e} PE {err:.2e} SubSteps {msubit+1} NegSteps {negcount} FailedConv {failedconv}")  
                negcount = 0
                failedconv = 0
                msubit = 0
            # Update state
            n, de0, de, lag, e, de0_sq = n1, de, de1, lag1, e1, de_sq
            n = normalize(n)
        return n
